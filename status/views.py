from django.shortcuts import render, redirect
from .models import Status
from .forms import StatusForm

# Create your views here.
def index(request):
    
    if request.method == "POST":
        form = StatusForm(request.POST)

        if form.is_valid():
            name = request.POST['name']
            message = request.POST['message']
            context = {
                'name' : name,
                'message' : message,
            }
            return render(request, 'confirm-status.html', context)

    else :
        form = StatusForm()
        status = Status.objects.order_by('-id')

        context = {
            'status' : status,
            'form' : form
        }
        return render(request, 'status.html', context)

def confirmStatus(request):
    if request.method == 'POST':
        
        if request.POST['confirm'] == 'yes':
            name = request.POST['name']
            message = request.POST['message']

            Status.objects.create(name=name, message=message)
            return redirect('status')

    return render(request, 'confirm-status.html')

