from django.urls import path
from .views import index, confirmStatus

urlpatterns = [
    path('', index, name='status'),
    path('confirmstatus/', confirmStatus, name='confirm-status')
]
