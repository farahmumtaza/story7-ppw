from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from .views import index, confirmStatus
from .models import Status
import time
from .apps import StatusConfig
from selenium.webdriver.chrome.options import Options
import os


# Create your tests here.
class UnitTestonStatus(TestCase):
    #LANDING PAGE TEST
    def test_does_landingpage_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_landingpage(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_does_index_views_show_correct_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'status.html')
    
    def test_check_landing_page_have_form(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)

    def test_save_a_POST_request(self):
        response = Client().post('/', data={'name':'PPW', 'message':'mengerjakan story'})
        self.assertEqual(response.status_code,200)

        


    #CONFIRMATION PAGE TEST
    def test_does_confirmation_page_exist(self):
        response = Client().get('/confirmstatus/')
        self.assertEqual(response.status_code, 200)
    
    def test_save_a_POST_request_2(self):
        response = Client().post('/confirmstatus/', data={'name':'PPW', 'message':'mengerjakan story', 'confirm': 'yes'})
        self.assertEqual(response.status_code,302)
    


    #CREATING OBJECTS
    def test_creating_an_object(self):
        status = Status.objects.create(name='Test', message='We are testing')
        self.assertEqual(Status.objects.count(), 1)

    def test_do_created_objects_show_up(self) :
        status = Status.objects.create(name='Test', message='We are testing')
        response = Client().get('/')
        # html_response = response.content.decode()
        # self.assertIn(response, 'Test')
        # self.assertIn(response, 'We are testing')
        self.assertContains(response, 'Test')
        self.assertContains(response, 'We are testing')


#FUNCTIONAL TEST
class FunctionalTestOnStatus(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)

        super(FunctionalTestOnStatus, self).setUp()

        # self.browser = webdriver.Chrome()

    def tearDown(self):
        self.browser.quit()
    
    def test_functionality(self):
        self.browser.get(self.live_server_url)
        time.sleep(1)

        self.browser.find_element_by_name('name').send_keys('Test')
        self.browser.find_element_by_name('message').send_keys('Currently unavailable')
        time.sleep(1)
        self.browser.find_element_by_name('submit').click()
        
        html_response = self.browser.page_source
        self.assertIn('Test', html_response)
        self.assertIn('Currently unavailable', html_response)
        time.sleep(1)
        self.browser.find_element_by_name('post').click()

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(StatusConfig.name, "status")

