from django import forms

class StatusForm(forms.Form):
    name = forms.CharField(max_length=20, label="Name", widget=forms.TextInput(attrs={
        'class':'form-control', 
        'placeholder' : 'Enter your name', 
        'style': '', 
        'required': 'True', 
        }))
    message = forms.CharField(max_length=255, label="Status", widget=forms.TextInput(attrs={
        'class':'form-control', 
        'placeholder' : 'What\'s up?', 
        'style': '',
        'required': 'True', 
        }))
